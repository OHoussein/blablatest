package dev.ohoussein.blablatest.injection.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.ohoussein.blablatest.injection.qualifier.ViewModelKey
import dev.ohoussein.blablatest.ui.viewmodel.TripListViewModel
import dev.ohoussein.blablatest.viewmodel.ViewModelFactory

@Module
interface ViewModelModule {


    @Binds
    @IntoMap
    @ViewModelKey(TripListViewModel::class)
    fun bindPostsViewModel(viewModel: TripListViewModel): ViewModel

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory


}