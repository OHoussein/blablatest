package dev.ohoussein.blablatest.injection.component

import androidx.lifecycle.ViewModelProvider
import dagger.Component
import dev.ohoussein.blablatest.data.repo.DataManager
import dev.ohoussein.blablatest.data.repo.ITripRepository
import dev.ohoussein.blablatest.injection.module.AppModule
import dev.ohoussein.blablatest.injection.module.DataModule
import dev.ohoussein.blablatest.injection.module.NetModule
import java.util.*
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class, DataModule::class])
interface AppComponent {

    fun postRepository(): ITripRepository

    fun dataManager(): DataManager

    fun viewModelFactory(): ViewModelProvider.Factory

    fun locale(): Locale

    fun currency(): Currency

}