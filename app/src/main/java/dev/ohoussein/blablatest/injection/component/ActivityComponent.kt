package dev.ohoussein.blablatest.injection.component

import dagger.Component
import dev.ohoussein.blablatest.injection.module.ActivityModule
import dev.ohoussein.blablatest.injection.scope.PerActivity
import dev.ohoussein.blablatest.ui.activity.TripListActivity

@PerActivity
@Component(dependencies = [AppComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(tripListActivity: TripListActivity)
}