package dev.ohoussein.blablatest.injection.qualifier

object QualifierNames {
    const val PREF_AUTH = "PREF_AUTH"
    const val OKHTTP_AUTH = "OKHTTP_AUTH"
}