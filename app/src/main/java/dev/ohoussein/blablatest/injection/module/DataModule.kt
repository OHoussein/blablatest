package dev.ohoussein.blablatest.injection.module

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dev.ohoussein.blablatest.Pref.AUTH_PREF
import dev.ohoussein.blablatest.data.api.AuthService
import dev.ohoussein.blablatest.data.api.BlaBlaCarService
import dev.ohoussein.blablatest.data.repo.AuthRepo
import dev.ohoussein.blablatest.data.repo.DataManager
import dev.ohoussein.blablatest.data.repo.IAuthRepo
import dev.ohoussein.blablatest.data.repo.ITripRepository
import dev.ohoussein.blablatest.injection.qualifier.QualifierNames
import java.util.*
import javax.inject.Named
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providePostRepository(service: BlaBlaCarService, locale: Locale, currency: Currency): ITripRepository =
            DataManager(service, locale, currency)

    @Provides
    fun provideDataManager(tripRepository: ITripRepository): DataManager = tripRepository as DataManager

    @Provides
    @Named(QualifierNames.PREF_AUTH)
    fun providePrefAuth(context: Context): SharedPreferences = context.getSharedPreferences(AUTH_PREF, Context.MODE_PRIVATE)

    @Provides
    fun provideAuthRepo(service: AuthService,
                        @Named(QualifierNames.PREF_AUTH) authPref: SharedPreferences): IAuthRepo = AuthRepo(service, authPref)
}