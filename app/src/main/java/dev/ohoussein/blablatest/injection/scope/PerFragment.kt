package dev.ohoussein.blablatest.injection.scope

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment
