package dev.ohoussein.blablatest.injection.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dev.ohoussein.blablatest.BuildConfig
import dev.ohoussein.blablatest.Config
import dev.ohoussein.blablatest.data.api.AuthService
import dev.ohoussein.blablatest.data.api.BlaBlaCarService
import dev.ohoussein.blablatest.data.api.HttpJsonHeadersInterceptor
import dev.ohoussein.blablatest.data.api.HttpTokenInterceptor
import dev.ohoussein.blablatest.data.gson.DurationDeserializer
import dev.ohoussein.blablatest.data.gson.LocalDateTimeDeserializer
import dev.ohoussein.blablatest.data.repo.IAuthRepo
import dev.ohoussein.blablatest.injection.qualifier.QualifierNames.OKHTTP_AUTH
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetModule {

    @Provides
    @Singleton
    fun provideGson(): Gson =
            GsonBuilder()
                    .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeDeserializer())
                    .registerTypeAdapter(Duration::class.java, DurationDeserializer())
                    .create()

    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }

    @Provides
    fun provideOkhttpClient(tokenInterceptor: HttpTokenInterceptor, logginInterceptor: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .connectTimeout(Config.HTTP_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(Config.HTTP_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .apply {
                        interceptors().addAll(listOf(tokenInterceptor, logginInterceptor))
                    }
                    .build()

    @Provides
    @Singleton
    fun provideBlaBlaCarService(gson: Gson, httpClient: OkHttpClient): BlaBlaCarService =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(Config.API_BASE_URL)
                    .validateEagerly(BuildConfig.DEBUG)
                    .client(httpClient)
                    .build()
                    .create(BlaBlaCarService::class.java)

    @Provides
    fun provideHttpJsonHeadersInterceptor() = HttpJsonHeadersInterceptor()

    @Provides
    @Named(OKHTTP_AUTH)
    fun provideAuthOkhttpClient(logginInterceptor: HttpLoggingInterceptor, jsonHeadersInterceptor: HttpJsonHeadersInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .connectTimeout(Config.HTTP_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(Config.HTTP_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .apply {
                        interceptors().addAll(listOf(logginInterceptor, jsonHeadersInterceptor))
                    }
                    .build()

    @Provides
    @Singleton
    fun provideAuthService(@Named(OKHTTP_AUTH) httpClient: OkHttpClient, gson: Gson): AuthService =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(Config.API_BASE_URL)
                    .validateEagerly(BuildConfig.DEBUG)
                    .client(httpClient)
                    .build()
                    .create(AuthService::class.java)

    @Provides
    @Singleton
    fun provideHttpTokenInterceptor(authRepo: IAuthRepo): HttpTokenInterceptor =
            HttpTokenInterceptor(authRepo)

}