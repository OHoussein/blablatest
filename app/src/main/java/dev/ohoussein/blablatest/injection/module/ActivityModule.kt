package dev.ohoussein.blablatest.injection.module

import android.app.Activity
import android.content.Context
import dagger.Module
import dagger.Provides
import dev.ohoussein.blablatest.injection.scope.PerActivity

@Module
class ActivityModule constructor(private val activity: Activity) {

    @Provides
    @PerActivity
    internal fun provideActivity() = activity

    @Provides
    @PerActivity
    internal fun provideContext(): Context = activity

}