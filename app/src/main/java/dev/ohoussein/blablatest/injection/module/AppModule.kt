package dev.ohoussein.blablatest.injection.module

import android.app.Application
import android.content.Context
import android.os.Build
import dagger.Module
import dagger.Provides
import java.util.*

@Module(includes = [ViewModelModule::class])
class AppModule(private val mApp: Application) {

    @Provides
    internal fun provideApplication() = mApp

    @Provides
    internal fun provideContext(): Context = mApp

    @Provides
    internal fun provideLocale(context: Context): Locale {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.resources.configuration.locales.get(0)
        } else {
            @Suppress("DEPRECATION")
            return context.resources.configuration.locale
        }
    }

    @Provides
    internal fun provideCurrency(locale: Locale): Currency = Currency.getInstance(locale)
}