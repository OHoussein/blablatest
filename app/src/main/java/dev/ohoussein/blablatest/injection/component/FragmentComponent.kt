package dev.ohoussein.blablatest.injection.component

import dagger.Component
import dev.ohoussein.blablatest.injection.module.FragmentModule
import dev.ohoussein.blablatest.injection.scope.PerFragment

@PerFragment
@Component(dependencies = [AppComponent::class], modules = [FragmentModule::class])
interface FragmentComponent {

}