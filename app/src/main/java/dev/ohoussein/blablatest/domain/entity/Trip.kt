package dev.ohoussein.blablatest.domain.entity

import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime

data class Trip(val id: String,
                val departureDate: LocalDateTime,
                val departurePlace: Place,
                val arrivalPlace: Place,
                val priceStr: String,
                val user: User,
                val duration: Duration
)