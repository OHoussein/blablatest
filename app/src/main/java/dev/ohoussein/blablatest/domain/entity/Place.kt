package dev.ohoussein.blablatest.domain.entity

data class Place(val city: String,
                 val address: String)