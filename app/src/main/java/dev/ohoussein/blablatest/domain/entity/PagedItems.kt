package dev.ohoussein.blablatest.domain.entity

data class PagedItems<T>(val items: List<T>,
                         val page: Page)