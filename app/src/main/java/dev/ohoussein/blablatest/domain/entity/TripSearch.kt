package dev.ohoussein.blablatest.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TripSearch(val departure: String, val destination: String) : Parcelable