package dev.ohoussein.blablatest.domain.entity

data class Page(val loadedPages: Int,
                val countPages: Int,
                val total: Int)