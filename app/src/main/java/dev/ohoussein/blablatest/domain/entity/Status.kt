package dev.ohoussein.blablatest.domain.entity

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
