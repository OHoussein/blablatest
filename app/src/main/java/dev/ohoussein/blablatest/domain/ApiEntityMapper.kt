package dev.ohoussein.blablatest.domain

import dev.ohoussein.blablatest.data.model.*
import dev.ohoussein.blablatest.domain.entity.*


fun ApiUser.toUser() = User(displayName, if (hasPicture) picture else null)

fun ApiPlace.toPlace() = Place(cityName, address)

fun ApiTrip.toTrip() = Trip(id = permanentId,
        departureDate = departureDate,
        departurePlace = departurePlace.toPlace(),
        arrivalPlace = arrivalPlace.toPlace(),
        user = user.toUser(),
        priceStr = price.valueStr,
        duration = duration)

fun ApiPage.toPage() = Page(
        loadedPages = page,
        countPages = pages,
        total = total
)

fun ApiTripList.toTripList() = PagedItems(
        page = pager.toPage(),
        items = trips.map { it.toTrip() }
)