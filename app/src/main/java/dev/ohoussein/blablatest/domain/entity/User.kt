package dev.ohoussein.blablatest.domain.entity

data class User(val displayName: String,
                val pictureUrl: String?)