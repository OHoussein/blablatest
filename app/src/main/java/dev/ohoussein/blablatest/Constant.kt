package dev.ohoussein.blablatest

object Pref {
    const val AUTH_PREF = "auth_pref"
    const val TOKEN_PREF = "token_pref"
}

object Extra {
    const val INPUT_TRIP_SEARCH = "input_trip_search"
}