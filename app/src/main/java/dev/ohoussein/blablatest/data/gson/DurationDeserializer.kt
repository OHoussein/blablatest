package dev.ohoussein.blablatest.data.gson

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.threeten.bp.Duration
import java.lang.reflect.Type

class DurationDeserializer : JsonDeserializer<Duration> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Duration {
        json.asJsonObject.apply {
            val value = get("value").asLong
            return when (get("unity").asString) {
                "ms" -> Duration.ofMillis(value)
                "m" -> Duration.ofMinutes(value)
                "h" -> Duration.ofHours(value)
                else -> Duration.ofSeconds(value)
            }
        }
    }
}