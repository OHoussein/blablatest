package dev.ohoussein.blablatest.data.repo

import dev.ohoussein.blablatest.data.api.BlaBlaCarService
import dev.ohoussein.blablatest.domain.toTripList
import dev.ohoussein.blablatest.testing.OpenForTesting
import java.util.*

@OpenForTesting
class DataManager(private val service: BlaBlaCarService,
                  private val locale: Locale,
                  private val currency: Currency) : ITripRepository {

    override suspend fun getTrips(fromAddress: String, toAddress: String, page: Int) =
            service.getTrips(fromAddress,
                    toAddress,
                    page,
                    locale.language,
                    currency.currencyCode)
                    .toTripList()
}