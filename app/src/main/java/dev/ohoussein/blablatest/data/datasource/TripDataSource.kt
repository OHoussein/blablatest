package dev.ohoussein.blablatest.data.datasource

import androidx.paging.PageKeyedDataSource
import dev.ohoussein.blablatest.data.repo.ITripRepository
import dev.ohoussein.blablatest.domain.entity.PagedItems
import dev.ohoussein.blablatest.domain.entity.Trip
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import timber.log.Timber

class TripDataSource(private val tripRepo: ITripRepository,
                     private val fromAddress: String,
                     private val toAddress: String,
                     private val scope: CoroutineScope) : PageKeyedDataSource<Int, Trip>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Trip>) {
        scope.launch {
            try {
                val pagedListTrip = tripRepo.getTrips(fromAddress, toAddress, 1)
                Timber.d("Got ${pagedListTrip.items.size} Trips")
                callback.onResult(pagedListTrip.items, null, pagedListTrip.page.loadedPages + 1)
            } catch (e: Exception) {
                Timber.e(e, "Error during loading trips")
                //TODO show error/retry
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Trip>) {
        Timber.d("loadAfter key:  ${params.key}")
        scope.launch {
            try {
                val pagedListTrip = tripRepo.getTrips(fromAddress, toAddress, params.key)
                Timber.d("Got ${pagedListTrip.items.size} Trips")
                callback.onResult(pagedListTrip.items, params.key + 1)
            } catch (e: Exception) {
                Timber.e(e, "Error during loading trips")
                //TODO show error/retry
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Trip>) {
    }

    private fun loadTrips(page: Int, postResult: (PagedItems<Trip>) -> Unit) {
        scope.launch {
            try {
                val pagedListTrip = tripRepo.getTrips(fromAddress, toAddress, page)
                Timber.d("Got ${pagedListTrip.items.size} Trips, page: $page")
                postResult.invoke(pagedListTrip)
            } catch (e: Exception) {
                Timber.e(e, "Error during loading trips")
                //TODO show error
            }
        }
    }

    override fun invalidate() {
        super.invalidate()
        scope.cancel()
    }
}