package dev.ohoussein.blablatest.data.gson

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import dev.ohoussein.blablatest.Config
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.lang.reflect.Type


class LocalDateTimeDeserializer : JsonDeserializer<LocalDateTime> {

    private val formatter = DateTimeFormatter.ofPattern(Config.DATE_TIME_FORMAT)

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): LocalDateTime {
        val strDate = json.asJsonPrimitive.asString
        return LocalDateTime.parse(strDate, formatter)
    }
}