package dev.ohoussein.blablatest.data.repo

import dev.ohoussein.blablatest.domain.entity.PagedItems
import dev.ohoussein.blablatest.domain.entity.Trip

interface ITripRepository {

    suspend fun getTrips(fromAddress: String, toAddress: String, page: Int): PagedItems<Trip>
}