package dev.ohoussein.blablatest.data.repo

interface IAuthRepo {
    fun retrieveToken(): String?
    suspend fun generateToken(): String

}