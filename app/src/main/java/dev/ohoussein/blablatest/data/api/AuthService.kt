package dev.ohoussein.blablatest.data.api

import dev.ohoussein.blablatest.data.model.ApiCredentialBody
import dev.ohoussein.blablatest.data.model.ApiToken
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST("token")
    suspend fun getToken(@Body credentialBody: ApiCredentialBody): ApiToken
}