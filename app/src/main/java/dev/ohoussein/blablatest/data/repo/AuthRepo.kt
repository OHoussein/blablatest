package dev.ohoussein.blablatest.data.repo

import android.content.SharedPreferences
import dev.ohoussein.blablatest.AuthConfig
import dev.ohoussein.blablatest.Pref.TOKEN_PREF
import dev.ohoussein.blablatest.data.api.AuthService
import dev.ohoussein.blablatest.data.model.ApiCredentialBody

class AuthRepo(private val service: AuthService,
               private val authPref: SharedPreferences
) : IAuthRepo {

    private val credentialBody = ApiCredentialBody(
            grantType = AuthConfig.GRANT_TYPE,
            clientId = AuthConfig.CLIENT_ID,
            secret = AuthConfig.CLIENT_SECRET,
            scopes = AuthConfig.SCOPES
    )

    override fun retrieveToken(): String? = authPref.getString(TOKEN_PREF, null)

    override suspend fun generateToken(): String =
            service.getToken(credentialBody)
                    .also {
                        authPref.edit().putString(TOKEN_PREF, it.accessToken).apply()
                    }.accessToken
}