package dev.ohoussein.blablatest.data.api

import dev.ohoussein.blablatest.AuthConfig
import dev.ohoussein.blablatest.data.repo.IAuthRepo
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber

class HttpTokenInterceptor(private val authRepo: IAuthRepo) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = runBlocking {
            authRepo.retrieveToken() ?: authRepo.generateToken()
        }
        val response = chain.proceed(addToken(chain, token))
        if (response.code != 401)
            return response

        Timber.w("Token is expired, regenerate a new one")
        val newToken = synchronized(this) {
            runBlocking { authRepo.generateToken() }
        }
        Timber.d("Got a new Token")
        return chain.proceed(addToken(chain, newToken))
    }

    private fun addToken(chain: Interceptor.Chain, token: String) = chain.request()
            .newBuilder()
            .header(AuthConfig.HEADER_AUTH, "${AuthConfig.HEADER_AUTH_TYPE} $token")
            .build()
}