package dev.ohoussein.blablatest.data.api

import dev.ohoussein.blablatest.data.model.ApiTripList
import retrofit2.http.GET
import retrofit2.http.Query

interface BlaBlaCarService {

    @GET("api/v2/trips?_format=json")
    suspend fun getTrips(
            @Query("fn") from: String,
            @Query("tn") to: String,
            @Query("page") page: Int,
            @Query("locale") locale: String,
            @Query("cur") currency: String): ApiTripList
}