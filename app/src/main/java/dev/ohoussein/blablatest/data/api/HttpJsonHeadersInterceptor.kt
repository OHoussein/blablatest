package dev.ohoussein.blablatest.data.api

import okhttp3.Interceptor
import okhttp3.Response

class HttpJsonHeadersInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request()
                .newBuilder()
                .header("Content-Type", "application/json")
                .build()
        )
    }
}