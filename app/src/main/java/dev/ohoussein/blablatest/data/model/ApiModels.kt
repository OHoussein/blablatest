package dev.ohoussein.blablatest.data.model

import com.google.gson.annotations.SerializedName
import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime

data class ApiTripList(@SerializedName("trips") val trips: List<ApiTrip>,
                       @SerializedName("pager") val pager: ApiPage)

data class ApiTrip(
        @SerializedName("permanent_id") val permanentId: String,
        @SerializedName("departure_date") val departureDate: LocalDateTime,
        @SerializedName("departure_place") val departurePlace: ApiPlace,
        @SerializedName("arrival_place") val arrivalPlace: ApiPlace,
        @SerializedName("price") val price: ApiPrice,
        @SerializedName("user") val user: ApiUser,
        @SerializedName("duration") val duration: Duration
)

data class ApiUser(@SerializedName("display_name") val displayName: String,
                   @SerializedName("picture") val picture: String,
                   @SerializedName("has_picture") val hasPicture: Boolean)

data class ApiPlace(@SerializedName("city_name") val cityName: String,
                    @SerializedName("address") val address: String)

data class ApiPrice(@SerializedName("string_value") val valueStr: String,
                    @SerializedName("price_color") val color: String)

data class ApiPage(@SerializedName("page") val page: Int,
                   @SerializedName("pages") val pages: Int,
                   @SerializedName("total") val total: Int
)

data class ApiToken(@SerializedName("access_token") val accessToken: String,
                    @SerializedName("issued_at") val issuedAt: Long,
                    @SerializedName("expires_in") val expiresIn: Long)

data class ApiCredentialBody(@SerializedName("grant_type") val grantType: String,
                             @SerializedName("client_id") val clientId: String,
                             @SerializedName("client_secret") val secret: String,
                             @SerializedName("scopes") val scopes: Array<String>)