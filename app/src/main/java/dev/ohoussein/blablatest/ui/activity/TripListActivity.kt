package dev.ohoussein.blablatest.ui.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import dev.ohoussein.blablatest.Extra
import dev.ohoussein.blablatest.R
import dev.ohoussein.blablatest.domain.entity.TripSearch
import dev.ohoussein.blablatest.ui.adapter.TripAdapter
import dev.ohoussein.blablatest.ui.base.BaseActivity
import dev.ohoussein.blablatest.ui.viewmodel.TripListViewModel
import kotlinx.android.synthetic.main.activity_trip_list.*
import timber.log.Timber
import javax.inject.Inject


class TripListActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var tripListViewModel: TripListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_list)

        component.inject(this)

        tripListViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(TripListViewModel::class.java)

        val adapter = TripAdapter()
        rv_posts.layoutManager = LinearLayoutManager(this)
        rv_posts.adapter = adapter

        val tripSearchRequest = intent.getParcelableExtra<TripSearch>(Extra.INPUT_TRIP_SEARCH)!!
        tripListViewModel.tripSearchRequest.value = tripSearchRequest
        tripListViewModel.tripList.observe(this, Observer {
            Timber.d("Livedata list size: ${it.size}")
            adapter.submitList(it)
        })

        supportActionBar?.apply {
            title = getString(R.string.trip_title, tripSearchRequest.departure, tripSearchRequest.destination)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
