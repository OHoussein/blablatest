package dev.ohoussein.blablatest.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import dev.ohoussein.blablatest.R
import dev.ohoussein.blablatest.domain.entity.Trip
import kotlinx.android.synthetic.main.item_trip.view.*
import org.threeten.bp.format.DateTimeFormatter

class TripAdapter : PagedListAdapter<Trip, TripAdapter.ViewHolder>(TripDiffCallback()) {

    private val timeFormat = DateTimeFormatter.ofPattern("HH:mm")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(
                R.layout.item_trip, parent, false), timeFormat)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }


    class ViewHolder(view: View, val timeFormat: DateTimeFormatter) : RecyclerView.ViewHolder(view) {
        private val tvDepTime: TextView = view.tvDepTime
        private val tvArrivalTime: TextView = view.tvArrivalTime
        private val tvDeparture: TextView = view.tvDeparture
        private val tvArrival: TextView = view.tvArrival
        private val tvPrice: TextView = view.tvPrice
        private val imgUser: ImageView = view.imgUser
        private val tvUserName: TextView = view.tvUserName

        fun bind(item: Trip) {
            with(item) {
                tvDepTime.text = departureDate.format(timeFormat)
                tvArrivalTime.text = departureDate.plus(duration).format(timeFormat)
                tvDeparture.text = departurePlace.city
                tvArrival.text = arrivalPlace.city
                tvPrice.text = priceStr
                tvUserName.text = user.displayName
                Glide.with(itemView.context)
                        .load(user.pictureUrl)
                        .placeholder(R.drawable.ic_user_placeholder)
                        .apply(RequestOptions.circleCropTransform())
                        .into(imgUser)
            }

        }
    }

    class TripDiffCallback : DiffUtil.ItemCallback<Trip>() {
        override fun areItemsTheSame(oldTrip: Trip, newTrip: Trip) = oldTrip.id == newTrip.id

        override fun areContentsTheSame(oldTrip: Trip, newTrip: Trip) = oldTrip == newTrip

    }
}