package dev.ohoussein.blablatest.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dev.ohoussein.blablatest.Extra
import dev.ohoussein.blablatest.R
import dev.ohoussein.blablatest.domain.entity.TripSearch
import dev.ohoussein.blablatest.ui.util.afterTextChanged
import kotlinx.android.synthetic.main.activity_trip_form.*

class TripFormActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_form)

        onInputsChanged()

        inputDestination.afterTextChanged {
            onInputsChanged()
        }
        inputDeparture.afterTextChanged {
            onInputsChanged()
        }

        fabValidate.setOnClickListener {
            val fromDeparture = inputDeparture.text.toString()
            val toDestination = inputDestination.text.toString()
            Intent(this, TripListActivity::class.java).apply {
                putExtra(Extra.INPUT_TRIP_SEARCH, TripSearch(fromDeparture, toDestination))
                startActivity(this)
            }
        }
    }

    private fun onInputsChanged() {
        if (inputDestination.text?.length ?: 0 < 3 || inputDeparture.text?.length ?: 0 < 3) {
            fabValidate.hide()
        } else {
            fabValidate.show()
        }
    }
}
