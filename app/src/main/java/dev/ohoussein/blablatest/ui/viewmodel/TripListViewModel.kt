package dev.ohoussein.blablatest.ui.viewmodel

import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import dev.ohoussein.blablatest.data.datasource.TripDataSource
import dev.ohoussein.blablatest.data.repo.DataManager
import dev.ohoussein.blablatest.domain.entity.Trip
import dev.ohoussein.blablatest.domain.entity.TripSearch
import javax.inject.Inject

class TripListViewModel @Inject constructor(
        private val dataManager: DataManager
) : ViewModel() {

    var tripSearchRequest = MutableLiveData<TripSearch>()

    val tripList: LiveData<PagedList<Trip>> = Transformations.switchMap(tripSearchRequest, ::getTrips)

    val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(3)
            .setPrefetchDistance(1)
            .build()

    private fun getTrips(tripSearch: TripSearch): LiveData<PagedList<Trip>> {
        val dataSourceFactory = object : DataSource.Factory<Int, Trip>() {
            override fun create(): DataSource<Int, Trip> {
                return TripDataSource(dataManager, tripSearch.departure, tripSearch.destination, viewModelScope)
            }
        }
        return LivePagedListBuilder<Int, Trip>(dataSourceFactory, pagedListConfig)
                .setInitialLoadKey(1)
                .build()
    }

}