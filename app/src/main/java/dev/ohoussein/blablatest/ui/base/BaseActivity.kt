package dev.ohoussein.blablatest.ui.base

import androidx.appcompat.app.AppCompatActivity
import dev.ohoussein.blablatest.getApp
import dev.ohoussein.blablatest.injection.component.ActivityComponent
import dev.ohoussein.blablatest.injection.component.DaggerActivityComponent

abstract class BaseActivity : AppCompatActivity() {

    val component: ActivityComponent by lazy {
        DaggerActivityComponent.builder()
                .appComponent(getApp().component)
                .build()
    }
}