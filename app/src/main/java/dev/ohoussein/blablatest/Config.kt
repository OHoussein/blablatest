package dev.ohoussein.blablatest

object Config {
    const val API_BASE_URL = "https://edge.blablacar.com"
    const val HTTP_CONNECTION_TIMEOUT: Long = 30 //in second
    const val DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss"
}

object AuthConfig {
    const val GRANT_TYPE = "client_credentials"
    //TODO get theses dynamically (firbase for example) for better security
    const val CLIENT_ID = "android-technical-tests"
    const val CLIENT_SECRET = "Y1oAL3QdPfVhGOWj3UeDjo3q02Qwhvrj"

    val SCOPES = arrayOf(
            "SCOPE_TRIP_DRIVER",
            "SCOPE_INTERNAL_CLIENT",
            "DEFAULT"
    )

    const val HEADER_AUTH = "Authorization"
    const val HEADER_AUTH_TYPE = "Bearer"
}