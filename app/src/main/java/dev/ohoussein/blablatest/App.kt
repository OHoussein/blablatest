package dev.ohoussein.blablatest

import android.app.Application
import android.content.Context
import androidx.annotation.VisibleForTesting
import com.jakewharton.threetenabp.AndroidThreeTen
import dev.ohoussein.blablatest.injection.component.AppComponent
import dev.ohoussein.blablatest.injection.component.DaggerAppComponent
import dev.ohoussein.blablatest.injection.module.AppModule
import dev.ohoussein.blablatest.injection.module.DataModule
import dev.ohoussein.blablatest.injection.module.NetModule
import timber.log.Timber

class App : Application() {

    lateinit var component: AppComponent
        @VisibleForTesting set

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule())
                .dataModule(DataModule())
                .build()
        AndroidThreeTen.init(this)
        Timber.plant(Timber.DebugTree())
    }
}

fun Context.getApp() = applicationContext as App