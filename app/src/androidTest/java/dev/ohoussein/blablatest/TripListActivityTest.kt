package dev.ohoussein.blablatest

import android.content.Intent
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import dev.ohoussein.blablatest.domain.entity.Page
import dev.ohoussein.blablatest.domain.entity.PagedItems
import dev.ohoussein.blablatest.domain.entity.TripSearch
import dev.ohoussein.blablatest.ui.activity.TripListActivity
import dev.ohoussein.blablatest.ui.adapter.TripAdapter
import io.mockk.coEvery
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.rules.TestRule
import org.junit.runner.RunWith


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class TripListActivityTest {


    private val component = TestComponentRule(ApplicationProvider.getApplicationContext<App>())

    @get:Rule
    val activity = ActivityTestRule<TripListActivity>(TripListActivity::class.java, false, false)

    // TestComponentRule needs to go first to make sure the Dagger ApplicationTestComponent is set
    // in the Application before any Activity is launched.
    @get:Rule
    val chain: TestRule = RuleChain.outerRule(component).around(activity)

    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    //
    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    //
    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun listOfPostsShows() {
        val testTrips = TestDataFactory.makeTrips(20)
        val tripSearch = TripSearch("Paris", "Amsterdam")
        coEvery {
            component.testComponent.dataManager().getTrips(tripSearch.departure, tripSearch.destination, 1)
        } returns PagedItems(testTrips, Page(1, 1, 20))

        val intent = Intent().apply {
            putExtra(Extra.INPUT_TRIP_SEARCH, tripSearch)
        }
        activity.launchActivity(intent)
        for (i in testTrips.indices) {
            val post = testTrips[i]

            onView(withId(R.id.rv_posts))
                    .perform(RecyclerViewActions.scrollToPosition<TripAdapter.ViewHolder>(i))
            onView(withText(post.departurePlace.city))
                    .check(matches(isDisplayed()))
        }
    }
}