package dev.ohoussein.blablatest.testing

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting