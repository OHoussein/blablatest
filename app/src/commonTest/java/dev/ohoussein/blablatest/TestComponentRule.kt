package dev.ohoussein.blablatest

import android.content.Context
import dev.ohoussein.blablatest.injection.AppTestComponent
import dev.ohoussein.blablatest.injection.DaggerAppTestComponent
import dev.ohoussein.blablatest.injection.DataTestModule
import dev.ohoussein.blablatest.injection.module.AppModule
import dev.ohoussein.blablatest.injection.module.NetModule
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class TestComponentRule(val context: Context) : TestRule {

    val testComponent: AppTestComponent =
            DaggerAppTestComponent.builder()
                    .appModule(AppModule(context.getApp()))
                    .netModule(NetModule())
                    .dataTestModule(DataTestModule())
                    .build()

    override fun apply(base: Statement, description: Description?): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                val application = context.getApp()
                application.component = testComponent
                base.evaluate()
            }
        }
    }
}