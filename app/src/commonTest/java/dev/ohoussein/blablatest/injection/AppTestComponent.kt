package dev.ohoussein.blablatest.injection

import dagger.Component
import dev.ohoussein.blablatest.injection.component.AppComponent
import dev.ohoussein.blablatest.injection.module.AppModule
import dev.ohoussein.blablatest.injection.module.NetModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetModule::class, DataTestModule::class])
interface AppTestComponent : AppComponent