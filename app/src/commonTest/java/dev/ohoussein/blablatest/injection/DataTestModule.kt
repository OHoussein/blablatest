package dev.ohoussein.blablatest.injection

import dagger.Module
import dagger.Provides
import dev.ohoussein.blablatest.data.repo.DataManager
import dev.ohoussein.blablatest.data.repo.ITripRepository
import io.mockk.mockk
import javax.inject.Singleton

@Module
class DataTestModule {


    @Provides
    @Singleton
    fun providePostRepository(): ITripRepository = mockk<DataManager>()

    @Provides
    fun provideDataManager(tripRepository: ITripRepository): DataManager = tripRepository as DataManager
}