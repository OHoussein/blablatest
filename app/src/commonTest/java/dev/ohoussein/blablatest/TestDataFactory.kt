package dev.ohoussein.blablatest

import dev.ohoussein.blablatest.domain.entity.Place
import dev.ohoussein.blablatest.domain.entity.Trip
import dev.ohoussein.blablatest.domain.entity.User
import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime
import java.util.concurrent.atomic.AtomicLong

/**
 * Created by Houssein OUERGHEMMI on 16/10/18.
 * ouerghemmi.houssein@gmail.com
 */

object TestDataFactory {
    private val idIndex = AtomicLong()

    fun makeFakeTrip(placeSuffix: String, userNameSuffix: String, timeDeltaHours: Long) = Trip(
            id = "id-${idIndex.getAndIncrement()}",
            departurePlace = Place("From place $placeSuffix", "from address $placeSuffix"),
            arrivalPlace = Place("To place $placeSuffix", "to address $placeSuffix"),
            departureDate = LocalDateTime.now().minusHours(timeDeltaHours),
            duration = Duration.ofMinutes((0..600L).random()),
            priceStr = "${(0..300).random()} €",
            user = makeFakeUser(userNameSuffix)
    )

    fun makeFakeUser(userNameSuffix: String) = User("User $userNameSuffix", null)

    fun makeTrips(count: Int): List<Trip> = ArrayList<Trip>(count).apply {
        for (i in 1..count)
            add(makeFakeTrip(i.toString(), i.toString(), i.toLong()))
    }
}